--CREATE SCHEMA "finance" AUTHORIZATION postgres;

CREATE sequence "finance".users_s
 START WITH 1000
 INCREMENT BY 1;
 
 
CREATE SEQUENCE "finance".user_type_s
 START WITH 1000
 INCREMENT BY 1;
 
CREATE SEQUENCE "finance".clients_s
 START WITH 1000
 INCREMENT BY 1;
 
CREATE SEQUENCE "finance".client_type_s
 START WITH 1000
 INCREMENT BY 1;
 
CREATE SEQUENCE "finance".transactions_s
 START WITH 1000
 INCREMENT BY 1;
 
CREATE SEQUENCE "finance".transactions_type_s
 START WITH 1000
 INCREMENT BY 1;
 
 CREATE SEQUENCE "finance".credit_deposit_s
 START WITH 1000
 INCREMENT BY 1;
 
 CREATE SEQUENCE "finance".credit_deposit_type_s
 START WITH 1000
 INCREMENT BY 1;
 
 CREATE SEQUENCE "finance".interest_s
 START WITH 1000
 INCREMENT BY 1;
 

CREATE TABLE finance.users (
	user_id int8 NOT NULL DEFAULT nextval('users_s'::regclass),
	user_name varchar(50) NOT NULL,
	first_name varchar(50) NULL,
	last_name varchar(50) NULL,
	email varchar(50) NULL,
	mobilenumber int8 NULL,
	address varchar(100) null,
	user_type_id int8 NULL,
	status int4 null,
	createdtime timestamp NOT NULL,
	updatedtime timestamp NOT NULL DEFAULT now(),
	CONSTRAINT users_pk PRIMARY KEY (user_id)
);

Alter sequence "finance".users_s owned by "finance".users.user_id;

CREATE TABLE finance.user_type (
	user_type_id int8 NOT NULL DEFAULT nextval('user_type_s'::regclass),
	role varchar(50) NOT NULL,
	status int4 null,
	createdtime timestamp NOT NULL,
	updatedtime timestamp NOT NULL DEFAULT now(),
	CONSTRAINT user_type_pk PRIMARY KEY (user_type_id)
);

Alter sequence "finance".user_type_s owned by "finance".user_type.user_type_id;


CREATE TABLE finance.clients (
	client_id int8 NOT NULL DEFAULT nextval('clients_s'::regclass),
	client_name varchar(50) NOT NULL,
	mobilenumber int8 null,
	family_member_name varchar(50) null,
	relation varchar(50) null,
	address varchar(50) null,
	aadhar varchar(50) null,
	pan varchar(50) null,
	client_type_id int8 null,
	status int4 null,
	createdtime timestamp NOT NULL,
	updatedtime timestamp NOT NULL DEFAULT now(),
	CONSTRAINT clients_pk PRIMARY KEY (client_id)
);

Alter sequence "finance".clients_s owned by "finance".clients.client_id;


CREATE TABLE finance.client_type (
	client_type_id int8 NOT NULL DEFAULT nextval('client_type_s'::regclass),
	client_type varchar(50) NOT NULL,
	status int4 null,
	createdtime timestamp NOT NULL,
	updatedtime timestamp NOT NULL DEFAULT now(),
	CONSTRAINT client_type_pk PRIMARY KEY (client_type_id)
);

Alter sequence "finance".client_type_s owned by "finance".client_type.client_type_id;

CREATE TABLE finance.transactions (
	transaction_id int8 NOT NULL DEFAULT nextval('transactions_s'::regclass),
	opening_balance int8 NOT NULL,
	datetime timestamp null,
	credit_deposit_id int8 null,
	interest_id int8 null,
	transaction_type_id int8 null,
	amount int8 null,
	description varchar(50) null,
	closing_balance int8 null,
	status int4 null,
	createdtime timestamp NOT NULL,
	updatedtime timestamp NOT NULL DEFAULT now(),
	CONSTRAINT transactions_pk PRIMARY KEY (transaction_id)
);

Alter sequence "finance".transactions_s owned by "finance".transactions.transaction_id;

CREATE TABLE finance.transactions_type (
	transaction_type_id int8 NOT NULL DEFAULT nextval('transactions_type_s'::regclass),
	transaction_type varchar(50) NOT NULL,
	status int4 null,
	createdtime timestamp NOT NULL,
	updatedtime timestamp NOT NULL DEFAULT now(),
	CONSTRAINT transaction_type_pk PRIMARY KEY (transaction_type_id)
);

Alter sequence "finance".transactions_type_s owned by "finance".transactions.transaction_type_id;

CREATE TABLE finance.credit_deposit (
	credit_deposit_id int8 NOT NULL DEFAULT nextval('credit_deposit_s'::regclass),
	client_id int8 NOT NULL,
	amount int8 null,
	credit_deposit_type_id int8 null,
	interest_perc double precision null,
	from_date timestamp null,
	to_date timestamp null,
	number_of_bonds int8 null,
	bond_id int8 null,
	status int4 null,
	createdtime timestamp NOT NULL,
	updatedtime timestamp NOT NULL DEFAULT now(),
	CONSTRAINT credit_deposit_pk PRIMARY KEY (credit_deposit_id)
);

Alter sequence "finance".credit_deposit_s owned by "finance".credit_deposit.credit_deposit_id;

CREATE TABLE finance.credit_deposit_type (
	credit_deposit_type_id int8 NOT NULL DEFAULT nextval('credit_deposit_type_s'::regclass),
	credit_deposit_type varchar(50) NOT NULL,
	status int4 null,
	createdtime timestamp NOT NULL,
	updatedtime timestamp NOT NULL DEFAULT now(),
	CONSTRAINT credit_deposit_type_pk PRIMARY KEY (credit_deposit_type_id)
);

Alter sequence "finance".credit_deposit_type_s owned by "finance".credit_deposit_type.credit_deposit_type_id;

CREATE TABLE finance.interest (
	interest_id int8 NOT NULL DEFAULT nextval('interest_s'::regclass),
	credit_deposit_id int8 null,
	interest_from_date timestamp null,
	interest_to_date timestamp null,
	paid_date timestamp null,
	generated_date timestamp null,
	status int4 null,
	createdtime timestamp NOT NULL,
	updatedtime timestamp NOT NULL DEFAULT now(),
	CONSTRAINT interest_pk PRIMARY KEY (interest_id)
);

Alter sequence "finance".interest_s owned by "finance".interest.interest_id;


ALTER TABLE "finance".users ADD CONSTRAINT users_fk 
FOREIGN KEY (user_type_id) REFERENCES "finance".user_type(user_type_id);

ALTER TABLE "finance".clients ADD CONSTRAINT clients_fk 
FOREIGN KEY (client_type_id) REFERENCES "finance".client_type(client_type_id);

ALTER TABLE "finance".clients ADD CONSTRAINT clients_fk 
FOREIGN KEY (client_type_id) REFERENCES "finance".client_type(client_type_id);

ALTER TABLE "finance".transactions ADD CONSTRAINT transactions_fk 
FOREIGN KEY (credit_deposit_id) REFERENCES "finance".credit_deposit(credit_deposit_id);

ALTER TABLE "finance".transactions ADD CONSTRAINT transactions_fk_1
FOREIGN KEY (interest_id) REFERENCES "finance".interest(interest_id);

ALTER TABLE "finance".transactions ADD CONSTRAINT transactions_fk_2
FOREIGN KEY (transaction_type_id) REFERENCES "finance".transactions_type(transaction_type_id);

ALTER TABLE "finance".credit_deposit ADD CONSTRAINT credit_deposit_fk
FOREIGN KEY (client_id) REFERENCES "finance".clients(client_id);

ALTER TABLE "finance".credit_deposit ADD CONSTRAINT credit_deposit_fk_1
FOREIGN KEY (credit_deposit_type_id) REFERENCES "finance".credit_deposit_type(credit_deposit_type_id);

ALTER TABLE "finance".interest ADD CONSTRAINT interest_fk
FOREIGN KEY (credit_deposit_id) REFERENCES "finance".credit_deposit(credit_deposit_id);




